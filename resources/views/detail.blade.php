@extends('layout')

@section('title')Новости@endsection

@section('main_content')
    <a href="{{ route('newsSection', ['section'=> $detail['section']->code]) }}"><h1 class="mb-5 text-dark">{{ $detail['section']->name }}</h1></a>
    <h2>{{ $detail['data']->name }}</h2>
    <small>{{ \Carbon\Carbon::parse($detail['data']->created_at)->format('d.m.Y H:i')}}</small>
    <hr/>
    <div>
        <div class="row">
            @if ($detail['data']->image)
                <div class="col-3">
                    <img class="w-100" src="{{ $detail['data']->image }}"/>
                </div>
            @endif
            <div class="col">
                {!! $detail['data']->detail !!}
            </div>
        </div>
    </div>
    <hr/>
    <div class="mb-5">
        Кол-во просмотров: <strong>{{  $detail['data']->views }}</strong>
    </div>
    @if ($popular AND $popular->isNotEmpty())
        <hr class="mt-5"/>
        <h2 class="mt-5">Самые просматриваемые новости</h2>
        @foreach($popular as $el)
            <ul>
                <li><a href="{{ route('newsItem', ['section'=> $el->section_code, 'news_id'=> $el->id, 'news_code'=> $el->code]) }}">{{ $el->name }}</a></li>
            </ul>
        @endforeach
    @endif
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to('/news');
});

Route::get('/news', 'NewsController@list')->name('newsList');
Route::get('/news/{section}', 'NewsController@section')->where('section', '[0-9a-z_-]+')->name('newsSection');
Route::get('/news/{section}/{news_id}-{news_code}.html', 'NewsController@detail')->where(['section' => '[0-9a-z_-]+','news_id' => '[0-9]+','news_code' => '[0-9a-z_-]+'])->name('newsItem');

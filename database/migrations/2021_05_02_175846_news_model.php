<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('state')->default(1);
            $table->string('name');
            $table->string('code');
            $table->string('image');
            $table->text('preview');
            $table->text('detail');
            $table->integer('views')->default(0);
            $table->integer('section');
        });

        Schema::create('news_category', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('state')->default(1);
            $table->string('name');
            $table->string('code')->index('code');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
        Schema::dropIfExists('news_category');
    }
}

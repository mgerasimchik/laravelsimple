<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NewsModel extends Model
{
    public function getSectionList()
    {
        $result=['list'=>DB::table('news_category')->select('id','name','code')->where('state', 1)->get(),'newsCnt'=>[]];

        $newsCnt = DB::table('news')->select(DB::raw('count(*) as news_count, section'))->where('state', 1)->groupBy('section')->get();

        foreach ($newsCnt as $item) $result['newsCnt'][$item->section]=$item->news_count;

        return $result;
    }

    public function getNewsList($section)
    {
        $result=['list'=>[]];
        $result['section']=DB::table('news_category')->select('id','name','code')->where('code', $section)->where('state', 1)->first();
        if($result['section']) {
            $result['list'] = DB::table('news')
                ->select('id', 'name', 'code', 'image', 'created_at', 'preview')
                ->where([
                    ['section', $result['section']->id],
                    ['state', 1],
                    ['created_at', '<=', date('Y-m-d H:i:s')]
                ])
                ->orderBy('created_at','desc')
                ->paginate(5);
        }
        return $result;
    }

    public function getNewsDetail($section,$news_id,$news_code)
    {
        $result=['data'=>[]];
        $result['section']=DB::table('news_category')->select('name','code')->where('code', $section)->where('state', 1)->first();
        if($result['section']) {
            $result['data'] = DB::table('news')
                ->select('id', 'name', 'image', 'created_at', 'detail','views')
                ->where([
                    ['id', $news_id],
                    ['state', 1],
                    ['created_at', '<=', date('Y-m-d H:i:s')]
                ])
                ->first();
        }
        return $result;
    }

    public function getPopularNews($cnt=3,$section='',$item_id=0)
    {
        $lastSevenDay = new \DateTime('-70 day');
        $where=[
            ['news.state', 1],
            ['news.created_at', '<=', date('Y-m-d H:i:s')],
            ['news.created_at', '>=', $lastSevenDay->format('Y-m-d H:i:s')]
        ];
        if($section) $where[]=['news_category.code', $section];
        if($item_id) $where[]=['news.id', '<>' ,$item_id];
        $result = DB::table('news')
            ->join('news_category', function ($join) {
                $join->on('news.section', '=', 'news_category.id')->where('news_category.state', 1);
            })
            ->select('news.id', 'news.name', 'news.code','news_category.code as section_code')
            ->where($where)
            ->orderBy('news.views','desc')
            ->limit($cnt)
            ->get();

        return $result;
    }

    public function addNewsView($news_id) {
        DB::table('news')->where('id', $news_id)->increment('views');
    }
}

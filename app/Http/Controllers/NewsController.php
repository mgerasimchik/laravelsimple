<?php

namespace App\Http\Controllers;

use App\NewsModel;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function list()
    {
        $news=new NewsModel();
        return view('list', ['section'=>$news->getSectionList(),'popular'=>$news->getPopularNews()]);
    }

    public function section($code)
    {
        $news=new NewsModel();
        $result = $news->getNewsList($code);
        if(!$result['list']) abort(404);
        return view('section', ['news'=>$result,'popular'=>$news->getPopularNews(3,$code)]);
    }

    public function detail($section,$news_id,$news_code)
    {
        $news=new NewsModel();

        // Просмотр новости без сессий
        $news->addNewsView($news_id);
        $result = $news->getNewsDetail($section,$news_id,$news_code);
        if(!$result['data']) abort(404);
        return view('detail', ['detail'=>$result,'popular'=>$news->getPopularNews(3,$section,$news_id)]);
    }
}

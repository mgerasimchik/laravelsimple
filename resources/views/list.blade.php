@extends('layout')

@section('title')Новости@endsection

@section('main_content')
    <h1>Категории</h1>
    @foreach($section['list'] as $el)
        <ul>
            <li><a href="{{ route('newsSection', ['section'=> $el->code]) }}">{{ $el->name }} ({{ (isset($section['newsCnt'][$el->id])?$section['newsCnt'][$el->id]:0) }})</a></li>
        </ul>
    @endforeach
    @if ($popular AND $popular->isNotEmpty())
        <hr class="mt-5"/>
        <h2 class="mt-5">Самые просматриваемые новости</h2>
        @foreach($popular as $el)
            <ul>
                <li><a href="{{ route('newsItem', ['section'=> $el->section_code, 'news_id'=> $el->id, 'news_code'=> $el->code]) }}">{{ $el->name }}</a></li>
            </ul>
        @endforeach
    @endif
@endsection

@extends('layout')

@section('title')Новости@endsection

@section('main_content')
    <h1>{{ $news['section']->name }}</h1>
    @foreach($news['list'] as $el)
        <div class="my-5">
            <div class="row">
                @if ($el->image)
                    <div class="col-3">
                        <img class="w-100" src="{{ $el->image }}"/>
                    </div>
                @endif
                <div class="col">
                    <h2>{{ $el->name }}</h2>
                    <small>{{ \Carbon\Carbon::parse($el->created_at)->format('d.m.Y H:i')}}</small>
                    <p>{{ $el->preview }}</p>
                    <a class="btn btm-sm btn-primary" href="{{ route('newsItem', ['section'=> $news['section']->code, 'news_id'=> $el->id, 'news_code'=> $el->code]) }}">Подробнее...</a>
                </div>
            </div>
        </div>
        <hr/>
    @endforeach
    <div class="mt-5">
        {{ $news['list']->links() }}
    </div>
    <div class="mb-5">
        <strong>Всего {{  $news['list']->total() }}</strong>
    </div>
    @if ($popular AND $popular->isNotEmpty())
        <hr class="mt-5"/>
        <h2 class="mt-5">Самые просматриваемые новости</h2>
        @foreach($popular as $el)
            <ul>
                <li><a href="{{ route('newsItem', ['section'=> $news['section']->code, 'news_id'=> $el->id, 'news_code'=> $el->code]) }}">{{ $el->name }}</a></li>
            </ul>
        @endforeach
    @endif
@endsection
